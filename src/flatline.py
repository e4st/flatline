#!/usr/bin/python3

import sys
import signal
import binascii

from lesspass.cli import parse_args
from lesspass.profile import create_profile
from lesspass.password import generate_password, _calc_entropy
import hashlib

import bip39.mnemonic_fork
# from i2c_master import *

# Handle unexpected exit
signal.signal(signal.SIGINT, lambda s, f: sys.exit(0))


def ascii_art():
    """Print tree ASCII art"""
    f = open("../banner/ascii_art.txt", "r")
    print(f.read())
    f.close()


def banner():
    """Print text banner"""
    f = open("../banner/banner.txt", "r")
    print(f.read())
    f.close()


def clear():
    """Clear terminal"""
    print(chr(27) + "[2J")


def create_password(args):
    func_args = args
    while True:
        if func_args.prompt:
            func_args.site = input("Site: ")
            func_args.login = input("Login: ")
            func_args.counter = input("Iteration [1]:")

        if func_args.counter == '':
            func_args.counter = 1
        if not func_args.site:
            print("ERROR argument SITE is required but was not provided.")
            continue

        profile, master_password = create_profile(func_args)
        generated_password = generate_password(profile, master_password)
        print(generated_password)
        # writeData(generated_password)
        break


def create_master_seed():
    with open('/dev/random', 'rb') as f:
        content = binascii.hexlify(f.read(16))
        f.close()

        data = binascii.unhexlify(content)
        m = bip39.mnemonic_fork.Mnemonic("english")
        mnem = m.to_mnemonic(data)
        seed = m.to_seed(mnem)
        return mnem, str(seed)


def create_bip39_seed(args):
    codeword = input("Codeword: ")
    iteration = input("Iteration [1]: ")

    if iteration == '':
        iteration = 1

    salt = (codeword + hex(iteration))

    entropy = binascii.hexlify(
        hashlib.pbkdf2_hmac(
            "sha256", args.master_password.encode("utf-8"), salt.encode("utf-8"), 100000, 32
        ))

    data = binascii.unhexlify(entropy)
    m = bip39.mnemonic_fork.Mnemonic("english")
    mnem = m.to_mnemonic(data)
    print(mnem)


def create_monero_seed(args):
    # Not yet implemented.
    pass


def create_ssh_key(args):
    # Not yet implemented.
    pass


def create_gpg_key(args):
    # Not yet implemented.
    pass


def create_shortlink(args):
    func_args = args
    while True:
        if func_args.prompt:
            func_args.ns = True
            func_args.site = input("Codename: ")
            func_args.counter = input("Iteration [1]: ")

        if func_args.counter == '':
            func_args.counter = 1

        if not func_args.site:
            print("ERROR argument CODENAME is required but was not provided.")
            continue

        profile, master_password = create_profile(func_args)
        generated_password = generate_password(profile, master_password)
        print(generated_password)
        # writeData(generated_password)
        func_args.ns = False
        break


def create_burner(args):
    while True:
        if args.prompt:
            sitename = input("Site: ")
            if sitename == '':
                print("ERROR argument SITE is required but was not provided.")
                continue

            codename = input("Codename [Leave blank for default]: ")
            args.site = sitename + codename

            print("Generating username and password...")
            profile, master_password = create_profile(args)
            generated_password = generate_password(profile, master_password)

            args.ns = True
            args.nu = True
            args.length = 8
            profile, master_password = create_profile(args)
            generated_username = generate_password(profile, master_password)

            print("Username: " + generated_username +
                  "\nPassword: " + generated_password)

            args.ns = False
            args.nu = False
            args.length = 16
        break


def main(args=sys.argv[1:]):
    """Main function"""
    args = parse_args(args)
    args.prompt = True

    while True:
        if not args.master_password:
            choice = input("Generate new master seed? [y/N]: ")
            if choice == 'y' or choice == 'Y':
                mnemonic, args.master_password = create_master_seed()
                print("Master seed [memorize or write it down]: " + "\n" + mnemonic + "\n")
            else:
                args.master_password = input("Master Seed: ")
                if args.master_password == '':
                    print('ERROR argument MASTER SEED is required but was not provided')
                    continue

        choice = int(input("Please select an option:\n"
                           "1: Create password\n"
                           "2: Create shortlink\n"
                           "3: Create burner account\n"
                           "4: Create BIP39 seed: \n"
                           "0: Exit\n"
                           "> "))

        if choice == 1:
            create_password(args)
        elif choice == 2:
            create_shortlink(args)
        elif choice == 3:
            create_burner(args)
        elif choice == 4:
            create_bip39_seed(args)
        elif choice == 0:
            exit()
        else:
            print("Invalid Choice")


if __name__ == '__main__':
    clear()
    banner()
    main()
