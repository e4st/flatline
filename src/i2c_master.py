#!/usr/bin/python3

import smbus

bus = smbus.SMBus(1)
address = 0x04


def write_data(value):
    byte_value = string_to_bytes(value)
    bus.write_i2c_block_data(address, 0x00, byte_value)
    return -1


def string_to_bytes(val):
    ret_val = []
    for c in val:
        ret_val.append(ord(c))
    return ret_val
